#include "yuv.h"

namespace colormath {

#define SCALEBITS 16
#define CENTERJSAMPLE 128
#define FIX(x) ((int32_t)((x) * (1L << SCALEBITS) + 0.5))
#define ONE_HALF ((int32_t)1 << (SCALEBITS-1))
#define CBCR_OFFSET ((int32_t)CENTERJSAMPLE << SCALEBITS)

#define clip255(x) (x > 255 ? 255 : (x < 0 ? 0 : x))

void rgb2yuv(uint8_t r, uint8_t g, uint8_t b, uint8_t& y, uint8_t& u, uint8_t& v) {
    y = (FIX(0.29900) * r + FIX(0.58700) * g + FIX(0.11400) * b + ONE_HALF) >> SCALEBITS;
    u = (-FIX(0.16874) * r - FIX(0.33126) * g + FIX(0.50000) * b + CBCR_OFFSET + ONE_HALF - 1) >> SCALEBITS;
    v = (FIX(0.50000) * r - FIX(0.41869) * g - FIX(0.08131) * b + CBCR_OFFSET + ONE_HALF - 1) >> SCALEBITS;
}

void yuv2rgb(uint8_t y, uint8_t u, uint8_t v, uint8_t& r, uint8_t& g, uint8_t& b) {
    int32_t scb = u - CENTERJSAMPLE;
    int32_t scr = v - CENTERJSAMPLE;
    r = clip255(y + ((ONE_HALF + FIX(1.40200) * scr) >> SCALEBITS));
    g = clip255(y + ((ONE_HALF - FIX(0.34414) * scb - FIX(0.71414) * scr) >> SCALEBITS));
    b = clip255(y + ((ONE_HALF + FIX(1.77200) * scb) >> SCALEBITS));
}

} // namespace colormath
