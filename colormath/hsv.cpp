#include "hsv.h"

namespace colormath {

void rgb2hsv(uint8_t r, uint8_t g, uint8_t b, uint32_t& h, uint16_t& s, uint8_t& v) {
    uint8_t max, mid, min;
    uint32_t edge;
    bool inverse;

    if (r > g) {
        if (g >= b) {
            max = r;
            mid = g;
            min = b;
            edge = 0;
            inverse = false;
        } else if (r > b) {
            max = r;
            mid = b;
            min = g;
            edge = 5;
            inverse = true;
        } else {
            max = b;
            mid = r;
            min = g;
            edge = 4;
            inverse = false;
        }
    } else if (r > b) {
        max = g;
        mid = r;
        min = b;
        edge = 1;
        inverse = true;
    } else {
        if (g > b) {
            max = g;
            mid = b;
            min = r;
            edge = 2;
            inverse = false;
        } else {
            max = b;
            mid = g;
            min = r;
            edge = 3;
            inverse = true;
        }
    }

    v = max;

    uint32_t delta = max - min;
    if (delta == 0) {
        s = 0;
        h = 0;
        return;
    }

    s = ((delta << 16) - 1) / v;

    h = (((mid - min) << 16) / delta) + 1;
    if (inverse)
        h = hueEdgeLen - h;
    h += edge * hueEdgeLen;
}

void hsv2rgb(uint32_t h, uint16_t s, uint8_t v, uint8_t& r, uint8_t& g, uint8_t& b) {
    if (s == 0 || v == 0) {
        r = v;
        g = v;
        b = v;
        return;
    }

    uint32_t delta = ((s * v) >> 16) + 1;
    uint8_t min = v - delta;
    uint8_t* mid;

    if (h >= hueEdgeLen * 4) {
        h -= hueEdgeLen * 4;
        if (h < hueEdgeLen) {
            b = v;
            g = min;
            mid = &r;
        } else {
            h -= hueEdgeLen;
            h = hueEdgeLen - h;
            r = v;
            g = min;
            mid = &b;
        }
    } else if (h >= hueEdgeLen * 2) {
        h -= hueEdgeLen * 2;
        if (h < hueEdgeLen) {
            g = v;
            r = min;
            mid = &b;
        } else {
            h -= hueEdgeLen;
            h = hueEdgeLen - h;
            b = v;
            r = min;
            mid = &g;
        }
    } else {
        if (h < hueEdgeLen) {
            r = v;
            b = min;
            mid = &g;
        } else {
            h -= hueEdgeLen;
            h = hueEdgeLen - h;
            g = v;
            b = min;
            mid = &r;
        }
    }

    *mid = ((h * delta) >> 16) + min;
}

} // namespace colormath
