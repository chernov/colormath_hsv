/*
 * Integer-based conversion between RGB and HSV color spaces.
 */

#ifndef HSV_H_
#define HSV_H_

#include <stdint.h>

namespace colormath {

const uint8_t maxValue = 0xFF;

const uint16_t maxSaturation = 0xFFFF;

const uint32_t hueEdgeLen = 65537;
const uint32_t maxHue = hueEdgeLen * 6;

/**
 * Converts RGB to HSV using integer based algorithm.
 *
 * @param[in]  r  red in range [0, 255]
 * @param[in]  g  green in range [0, 255]
 * @param[in]  b  blue in range [0, 255]
 * @param[out] h  hue in range [0, maxHue]
 * @param[out] s  saturation in range [0, maxSaturation]
 * @param[out] v  value in range [0, maxValue]
 */
void rgb2hsv(uint8_t r, uint8_t g, uint8_t b, uint32_t& h, uint16_t& s, uint8_t& v);

/**
 * Converts HSV to RGB using integer based algorithm.
 *
 * @param[in]  h  hue in range [0, maxHue]
 * @param[in]  s  saturation in range [0, maxSaturation]
 * @param[in]  v  value in range [0, maxValue]
 * @param[out] r  red in range [0, 255]
 * @param[out] g  green in range [0, 255]
 * @param[out] b  blue in range [0, 255]
 */
void hsv2rgb(uint32_t h, uint16_t s, uint8_t v, uint8_t& r, uint8_t& g, uint8_t& b);

} // namespace colormath

#endif // HSV_H_
