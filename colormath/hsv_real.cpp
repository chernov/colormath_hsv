#include "hsv_real.h"

namespace colormath {

void rgb2hsv_real(float r, float g, float b, float& h, float& s, float& v) {
    float min = (r < g) ? r : g;
    min = (min < b) ? min : b;

    float max = (r > g) ? r : g;
    max = (max > b) ? max : b;

    v = max;

    float delta = max - min;
    if (delta < 0.0001f) {
        s = 0.0f;
        h = 0.0f;
        return;
    }

    s = delta / max;

    if (max == r) {
        h = (g - b) / delta;
        if (h < 0.0f)
            h += 6.0f;
    } else if (max == g)
        h = 2.0f + (b - r) / delta;
    else
        h = 4.0f + (r - g) / delta;
    h /= 6.0f;
}

void hsv2rgb_real(float h, float s, float v, float& r, float& g, float& b) {
    if (s < 0.0001f) {
        r = v;
        g = v;
        b = v;
        return;
    }

    h *= 6.0f;
    int i = (int)h;
    float d = h - i;

    float w = v * (1.0f - s);
    float q = v * (1.0f - (s * d));
    float t = v * (1.0f - (s * (1.0f - d)));

    switch (i) {
    case 0:
        r = v;
        g = t;
        b = w;
        break;
    case 1:
        r = q;
        g = v;
        b = w;
        break;
    case 2:
        r = w;
        g = v;
        b = t;
        break;
    case 3:
        r = w;
        g = q;
        b = v;
        break;
    case 4:
        r = t;
        g = w;
        b = v;
        break;
    case 5:
    default:
        r = v;
        g = w;
        b = q;
        break;
    }
}

} // namespace colormath
