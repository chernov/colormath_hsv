/*
 * Conversion between RGB and YUV (YCbCr) color spaces.
 */

#ifndef YUV_H_
#define YUV_H_

#include <stdint.h>

namespace colormath {

/**
 * Converts RGB to YUV (YCbCr).
 *
 * @param[in]  r  red
 * @param[in]  g  green
 * @param[in]  b  blue
 * @param[out] y  luminance
 * @param[out] u  chrominance-blue, aka Cb
 * @param[out] v  chrominance-red, aka Cr
 */
void rgb2yuv(uint8_t r, uint8_t g, uint8_t b, uint8_t& y, uint8_t& u, uint8_t& v);

/**
 * Converts YUV (YCbCr) to RGB.
 *
 * @param[in]  y  luminance
 * @param[in]  u  chrominance-blue, aka Cb
 * @param[in]  v  chrominance-red, aka Cr
 * @param[out] r  red
 * @param[out] g  green
 * @param[out] b  blue
 */
void yuv2rgb(uint8_t y, uint8_t u, uint8_t v, uint8_t& r, uint8_t& g, uint8_t& b);

} // namespace colormath

#endif // YUV_H_
