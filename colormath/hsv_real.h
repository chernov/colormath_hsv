/*
 * Real-valued conversion between RGB and HSV color spaces.
 */

#ifndef HSV_REAL_H_
#define HSV_REAL_H_

namespace colormath {

/**
 * Converts RGB to HSV using real-valued algorithm.
 * All arguments are in range [0, 1].
 *
 * @param[in]  r  red
 * @param[in]  g  green
 * @param[in]  b  blue
 * @param[out] h  hue
 * @param[out] s  saturation
 * @param[out] v  value
 */
void rgb2hsv_real(float r, float g, float b, float& h, float& s, float& v);

/**
 * Converts HSV to RGB using real-valued algorithm.
 * All arguments are in range [0, 1].
 *
 * @param[in]  h  hue
 * @param[in]  s  saturation
 * @param[in]  v  value
 * @param[out] r  red
 * @param[out] g  green
 * @param[out] b  blue
 */
void hsv2rgb_real(float h, float s, float v, float& r, float& g, float& b);

} // namespace colormath

#endif // HSV_REAL_H_
