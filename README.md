# colormath #

Color manipulation library. Contains implementation of the integer-based accurate conversion algorithm between HSV and RGB color spaces.