#include "liu.h"

#include <math.h>

int ScaleTab[256];
int N;
int scalor;

void init_liu() {
    N = 2;
    scalor = pow(2, N);
    for (int delta = 1; delta <= 255; delta++) {
        ScaleTab[delta - 1] = (int)(scalor / delta + 0.5f);
    }
}

void rgb2hsv_liu(uint8_t r, uint8_t g, uint8_t b, int& h, int& s, int& v) {
    uint8_t min = (r < g) ? r : g;
    min = (min < b) ? min : b;

    uint8_t max = (r > g) ? r : g;
    max = (max > b) ? max : b;

    int delta = max - min;
    if (delta == 0) {
        s = 0;
        h = 0;
        return;
    }

    v = max;

    s = (scalor * delta * ScaleTab[v]) >> N;

    if (max == r) {
        h = (60 * (g - b) * ScaleTab[delta - 1]) >> N;
    }
    else if (max == g) {
        h = 120 + ((60 * (b - r) * ScaleTab[delta - 1]) >> N);
    }
    else {
        h = 240 + ((60 * (r - g) * ScaleTab[delta - 1]) >> N);
    }
}

