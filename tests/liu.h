#ifndef LIU_H_
#define LIU_H_

#include <stdint.h>

void init_liu();

void rgb2hsv_liu(uint8_t r, uint8_t g, uint8_t b, int& h, int& s, int& v);

#endif // LIU_H_

