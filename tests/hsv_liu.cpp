#include <chrono>
#include <iostream>
#include <limits>
#include <stdint.h>
#include <stdio.h>

#include "liu.h"
#include "../colormath/hsv.h"

void test_hsv() {
    using namespace colormath;

    for (uint32_t c = 0; c <= 0xFFFFFF; c++) {
        uint8_t redOrig = (c >> 16);
        uint8_t greenOrig = (c >> 8);
        uint8_t blueOrig = c;

        uint32_t hue;
        uint16_t saturation;
        uint8_t value;
        rgb2hsv(redOrig, greenOrig, blueOrig, hue, saturation, value);
    }
}

void test_hsv_liu() {
    for (uint32_t c = 0; c <= 0xFFFFFF; c++) {
        uint8_t redOrig = (c >> 16);
        uint8_t greenOrig = (c >> 8);
        uint8_t blueOrig = c;

        int hue;
        int saturation;
        int value;
        rgb2hsv_liu(redOrig, greenOrig, blueOrig, hue, saturation, value);
    }
}

int main(int argc, char** argv) {
    int iterationsCount = 1;

    if (argc > 1) {
        char* iterationsCountVal = argv[1];
        sscanf(iterationsCountVal, "%d", &iterationsCount);
    }

    long long bestTimeInt = std::numeric_limits<long long>::max();
    long long bestTimeLiu = std::numeric_limits<long long>::max();

    // integer-based algorithm
    {
        std::cout << "Integer algorithm" << std::endl;

        for (int i = 0; i < iterationsCount; i++) {
            auto start = std::chrono::high_resolution_clock::now();
            test_hsv();
            auto end = std::chrono::high_resolution_clock::now();

            auto dt = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
            if (dt < bestTimeInt)
                bestTimeInt = dt;

            std::cout << "Iteration " << (i + 1) << " time: " << dt << " ms" << std::endl;
        }
    }

    // Liu et al. algorithm
    {
        std::cout << "Liu et al. algorithm" << std::endl;

        init_liu();
        for (int i = 0; i < iterationsCount; i++) {
            auto start = std::chrono::high_resolution_clock::now();
            test_hsv_liu();
            auto end = std::chrono::high_resolution_clock::now();

            auto dt = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
            if (dt < bestTimeLiu)
                bestTimeLiu = dt;

            std::cout << "Iteration " << (i + 1) << " time: " << dt << " ms" << std::endl;
        }
    }

    std::cout << "Best time: " << bestTimeInt << " ms" << std::endl;
    std::cout << "Best time for Liu algorithm: " << bestTimeLiu << " ms" << std::endl;
    return 0;
}
