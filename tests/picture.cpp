﻿#if defined(_WIN32)
#include <Windows.h>
#endif
#include <iostream>
#include <stdint.h>
#include <string>
#include <string.h>

#include <tiffio.h>

#include "../colormath/hsv.h"

int main(int argc, char** argv) {
    using namespace colormath;

    std::string fn;
    if (argc > 1) {
        fn = argv[1];
    }
    if (fn.empty()) {
        return 0;
    }

    int width = 4096;
    int heightValue = 1024;
    int heightSaturation = 1024;
    int height = heightValue + heightSaturation;
    int samplePerPixel = 3;
    uint8_t* img = new uint8_t[width * height * samplePerPixel];

    int yStart = 0;
    for (int y = yStart, end = yStart + heightSaturation; y < end; y++) {
        int row = y * width * samplePerPixel;

        for (int x = 0; x < width; x++) {
            int p = row + x * samplePerPixel;
            uint8_t& r = img[p];
            uint8_t& g = img[p + 1];
            uint8_t& b = img[p + 2];

            float hueReal = (float)x / width;
            float saturationReal = (float)(y - yStart) / heightSaturation;

            uint32_t hue = (uint32_t)(hueReal * maxHue);
            uint16_t saturation = (uint16_t)(saturationReal * maxSaturation);
            uint8_t value = maxValue;

            hsv2rgb(hue, saturation, value, r, g, b);
        }
    }

    yStart = heightSaturation;
    for (int y = yStart, end = yStart + heightValue; y < end; y++) {
        int row = y * width * samplePerPixel;

        for (int x = 0; x < width; x++) {
            int p = row + x * samplePerPixel;
            uint8_t& r = img[p];
            uint8_t& g = img[p + 1];
            uint8_t& b = img[p + 2];

            float hueReal = (float)x / width;
            float valueReal = 1.0f - (float)(y - yStart) / heightValue;
            float saturationReal = 1.0f;

            uint32_t hue = (uint32_t)(hueReal * maxHue);
            uint16_t saturation = maxSaturation;
            uint8_t value = (uint8_t)(valueReal * maxValue);

            hsv2rgb(hue, saturation, value, r, g, b);
        }
    }

    TIFF* tif = TIFFOpen(fn.c_str(), "w");

    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, width);
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, height);
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, samplePerPixel);
    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8);
    TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
    TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
    TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
    TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tif, width * samplePerPixel));

    size_t lineSize = TIFFScanlineSize(tif);
    uint8_t* line = (uint8_t*)_TIFFmalloc(lineSize);

    for (int row = 0; row < height; row++) {
        memcpy(line, &img[row * lineSize], lineSize);
        if (TIFFWriteScanline(tif, line, row, 0) < 0) {
            break;
        }
    }

    _TIFFfree(line);
    TIFFClose(tif);
    delete[] img;
    return 0;
}
