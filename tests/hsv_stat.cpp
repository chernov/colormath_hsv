/*
 * Collects statistics of HSV colors.
 */

#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <stdint.h>
#include <vector>

#include "../colormath/hsv.h"
#include "../colormath/hsv_real.h"

struct HSV {
    HSV(uint32_t h, uint16_t s, uint8_t v) : h(h), s(s), v(v) {}

    bool operator<(const HSV& other) const {
        if (v != other.v)
            return v < other.v;
        if (s != other.s)
            return s < other.s;
        return h < other.h;
    }

    uint32_t h;
    uint16_t s;
    uint8_t v;
};

/*
 * Calculates greatest common divisor.
 */
uint8_t gcd(uint8_t a, uint8_t b) {
    if (b == 0)
        return a;
    return gcd(b, a % b);
}

struct Fraction {
    Fraction() : numerator(0), denominator(0) {}

    Fraction(uint8_t numerator, uint8_t denominator) {
        this->numerator = numerator;
        this->denominator = denominator;
        reduce();
    }

    void reduce() {
        uint8_t d = gcd(numerator, denominator);
        numerator /= d;
        denominator /= d;
    }

    bool operator<(const Fraction& other) const {
        return (int)(numerator * other.denominator) < (int)(denominator * other.numerator);
    }

    bool operator!=(const Fraction& other) const {
        return (numerator != other.numerator) || (denominator != other.denominator);
    }

    uint8_t numerator;
    uint8_t denominator;
};

std::ostream& operator<<(std::ostream& stream, const Fraction& fraction) {
    return stream << (int)fraction.numerator << "/" << (int)fraction.denominator;
}

typedef std::set<uint32_t> IntSet;
typedef std::map<Fraction, IntSet> FractionIntMap;

struct SaturationData {
    Fraction fraction;
    uint8_t deltaOrig;
    uint32_t deltaObtained;
    uint8_t max;
    float real;
};

typedef std::vector<SaturationData> SaturationDataContainer;
typedef std::map<uint32_t, SaturationDataContainer> SaturationIntDataMap;

struct HueData {
    Fraction fraction;
    uint8_t midOrig;
    uint32_t midObtained;
    uint8_t min;
    uint8_t delta;
    float real;
};

typedef std::vector<HueData> HueDataContainer;
typedef std::map<uint32_t, HueDataContainer> HueIntDataMap;

/*
 * Calculates minimum number of bits needed to represent the number.
 */
int calcBits(uint32_t num) {
    int bits = 1;
    for (int i = 0; i < 32; i++) {
        uint32_t mask = 1 << i;
        if ((num & mask) != 0)
            bits = i + 1;
    }
    return bits;
}

void printBits(std::ostream& stream, uint32_t num, int bits) {
    for (int i = bits - 1; i >= 0; i--) {
        uint32_t mask = 1 << i;
        char c = (num & mask) == 0 ? '0' : '1';
        stream << c;
    }
    stream << '(' << num << ')';
}

/*
 * Collects data while enumerating all possible saturations.
 */
void enumerateSaturations(FractionIntMap& satFractIntMap,
                          SaturationIntDataMap& satIntDataMap,
                          int& bits) {
    bits = 1;

    for (int maxInd = 1; maxInd <= 255; maxInd++) {
        uint8_t max = (uint8_t)maxInd;
        float maxReal = (float)max * (1.0f / 255.0f);

        for (int minInd = 0; minInd < maxInd; minInd++) {
            uint8_t min = (uint8_t)minInd;
            float minReal = (float)min * (1.0f / 255.0f);

            assert(max > min);
            uint8_t delta = max - min;

            // forward
            uint32_t s;
            s = delta;
            s <<= 16;
            s -= 1;
            s /= max;

            Fraction fract(delta, max);
            satFractIntMap[fract].insert(s);

            // backward
            uint32_t deltaObtained;
            deltaObtained = s;
            deltaObtained *= max;
            deltaObtained >>= 16;
            deltaObtained += 1;
            deltaObtained &= 0xFF;

            float deltaReal = maxReal - minReal;
            float saturationReal = (deltaReal != 0.0f) ? deltaReal / maxReal : 0.0f;

            SaturationData data;
            data.fraction = fract;
            data.deltaOrig = delta;
            data.deltaObtained = deltaObtained;
            data.max = max;
            data.real = saturationReal;
            satIntDataMap[s].push_back(data);

            int maxBits = calcBits(s);
            if (maxBits > bits)
                bits = maxBits;
        }
    }
}

/*
 * Collects data while enumerating all possible hues.
 */
void enumerateHues(FractionIntMap& hueFractIntMap,
                   HueIntDataMap& hueIntDataMap,
                   int& bits) {
    bits = 0;

    for (int maxInd = 1; maxInd <= 255; maxInd++) {
        uint8_t max = (uint8_t)maxInd;
        float maxReal = (float)max * (1.0f / 255.0f);

        for (int minInd = 0; minInd < maxInd; minInd++) {
            uint8_t min = (uint8_t)minInd;
            float minReal = (float)min * (1.0f / 255.0f);

            for (int midInd = minInd; midInd <= maxInd; midInd++) {
                uint8_t mid = (uint8_t)midInd;
                float midReal = (float)mid * (1.0f / 255.0f);

                assert(max > min);
                assert(mid <= max);
                assert(mid >= min);
                uint8_t delta = max - min;

                // forward
                uint32_t h;
                h = mid - min;
                h <<= 16;
                h /= delta;
                h += 1;

                Fraction fract(mid - min, delta);
                hueFractIntMap[fract].insert(h);

                // backward
                uint32_t midObtained;
                midObtained = h;
                midObtained *= delta;
                midObtained >>= 16;
                midObtained += min;
                midObtained &= 0xFF;

                float deltaReal = maxReal - minReal;
                float hueReal = (deltaReal != 0.0f) ? (midReal - minReal) / deltaReal : 0.0f;

                HueData data;
                data.fraction = fract;
                data.midOrig = mid;
                data.midObtained = midObtained;
                data.min = min;
                data.delta = delta;
                data.real = hueReal;
                hueIntDataMap[h].push_back(data);

                int maxBits = calcBits(h);
                if (maxBits > bits)
                    bits = maxBits;
            }
        }
    }
}

/*
 * Calculates number of dualities.
 * In this test, the duality is a second or subsequent integer component
 * that is associated with single real-valued component.
 */
size_t calcDualities(const FractionIntMap& realIntMap) {
    size_t count = 0;
    for (FractionIntMap::const_iterator it = realIntMap.begin(), end = realIntMap.end();
            it != end; ++it) {
        const IntSet& integers = it->second;
        size_t size = integers.size();
        if (size > 1)
            count += size - 1;
    }
    return count;
}

/*
 * Calculates the number of errors in the data.
 * Prints information about errors if stream is not null.
 */
template<class DataT>
size_t checkDataErrors(const DataT& data, uint32_t integer, Fraction fract, std::ostream* stream);

/*
 * Error check specialization for saturation.
 */
template<>
size_t checkDataErrors<SaturationData>(const SaturationData& data,
                                       uint32_t integer,
                                       Fraction fract,
                                       std::ostream* stream) {
    size_t errors = 0;

    // backward conversion is wrong
    if (data.deltaObtained != data.deltaOrig) {
        errors++;
        if (stream != NULL) {
            *stream << " delta:";
            printBits(*stream, data.deltaObtained, 8);
            *stream << "!=";
            printBits(*stream, data.deltaOrig, 8);
        }
    }

    // duplicate
    if (data.fraction != fract) {
        errors++;
        if (stream != NULL) {
            *stream << " duplicate:";
            *stream << (int)data.deltaOrig << "/" << (int)data.max;
        }
    }

    return errors;
}

/*
 * Error check specialization for hue.
 */
template<>
size_t checkDataErrors<HueData>(const HueData& data,
                                uint32_t integer,
                                Fraction fract,
                                std::ostream* stream) {
    size_t errors = 0;

    // backward conversion is wrong
    if (data.midObtained != data.midOrig) {
        errors++;
        if (stream != NULL) {
            *stream << " mid:";
            printBits(*stream, data.midObtained, 8);
            *stream << "!=";
            printBits(*stream, data.midOrig, 8);
        }
    }

    // duplicate
    if (data.fraction != fract) {
        errors++;
        if (stream != NULL) {
            *stream << " duplicate:";
            *stream << "(" << (int)data.midOrig << "-" << (int)data.min << ")";
            *stream << "/" << (int)data.delta;
        }
    }

    return errors;
}

/*
 * Prints data, returns number of errors.
 */
template<class IntDataMapT>
int printData(std::ostream& stream,
              const FractionIntMap& fractIntMap,
              const IntDataMapT& intDataMap,
              int bits,
              size_t errorsPrintLim) {
    typedef typename IntDataMapT::mapped_type DataContainer;
    typedef typename DataContainer::value_type Data;

    int errorsCnt = 0;
    FractionIntMap::const_iterator fractIntIt = fractIntMap.begin();
    FractionIntMap::const_iterator fractIntEnd = fractIntMap.end();
    for (; fractIntIt != fractIntEnd; ++fractIntIt) {
        Fraction fract = fractIntIt->first;
        const IntSet& integers = fractIntIt->second;

        stream << fract;

        if (integers.size() > 1)
            stream << "\tdualities:\n";
        IntSet::const_iterator intIt = integers.begin();
        IntSet::const_iterator intEnd = integers.end();
        for (; intIt != intEnd; ++intIt) {
            uint32_t integer = *intIt;

            typename IntDataMapT::const_iterator intDataIt = intDataMap.find(integer);
            if (intDataIt == intDataMap.end()) {
                std::cerr << "data not found for integer: " << integer << std::endl;
                continue;
            }
            const DataContainer& dataContainer = intDataIt->second;

            stream << '\t';
            printBits(stream, integer, bits);

            size_t dataErrors = 0;
            typename DataContainer::const_iterator dataIt = dataContainer.begin();
            typename DataContainer::const_iterator dataEnd = dataContainer.end();
            for (; dataIt != dataEnd; ++dataIt) {
                const Data& data = *dataIt;

                size_t errors = checkDataErrors(data, integer, fract,
                                                (dataErrors < errorsPrintLim) ? &stream : NULL);
                dataErrors += errors;
                errorsCnt += errors;
            }

            if (dataErrors > errorsPrintLim)
                stream << " ... " << (dataErrors - errorsPrintLim) << " more errors omitted";

            stream << '\n';
        }
    }

    return errorsCnt;
}

int main(int argc, char** argv) {
    using namespace colormath;

    std::string saturationLog;
    std::string hueLog;
    if (argc > 1)
        saturationLog = argv[1];
    if (argc > 2)
        hueLog = argv[2];

    bool fail = false;

    // saturation

    FractionIntMap satFractIntMap;
    SaturationIntDataMap satIntDataMap;
    int satBits;
    enumerateSaturations(satFractIntMap, satIntDataMap, satBits);

    std::cout << "Saturation" << std::endl;
    std::cout << "fractions count: " << satFractIntMap.size() << std::endl;

    std::cout << "integers count: " << satIntDataMap.size();
    if (satFractIntMap.size() != satIntDataMap.size()) {
        std::cout << " (not correct)";
        fail = true;
    }
    std::cout << std::endl;

    size_t satDualities = calcDualities(satFractIntMap);
    if (satDualities > 0)
        fail = true;
    std::cout << "dualities: " << satDualities << std::endl;

    std::cout << "bits: " << satBits << std::endl;

    if (!saturationLog.empty()) {
        std::ofstream file(saturationLog.c_str());

        size_t errorsCnt = printData(file, satFractIntMap, satIntDataMap, satBits, 3);
        if (errorsCnt > 0)
            fail = true;
        std::cout << "errors: " << errorsCnt << std::endl;

        file.close();
    }

    // hue

    FractionIntMap hueFractIntMap;
    HueIntDataMap hueIntDataMap;
    int hueBits;
    enumerateHues(hueFractIntMap, hueIntDataMap, hueBits);

    std::cout << "Hue" << std::endl;
    std::cout << "fractions count: " << hueFractIntMap.size() << std::endl;

    std::cout << "integers count: " << hueIntDataMap.size();
    if (hueFractIntMap.size() != hueIntDataMap.size())
        std::cout << " (not correct)";
    std::cout << std::endl;

    size_t hueDualities = calcDualities(hueFractIntMap);
    if (hueDualities > 0)
        fail = true;
    std::cout << "dualities: " << hueDualities << std::endl;

    std::cout << "bits: " << hueBits << std::endl;

    if (!hueLog.empty()) {
        std::ofstream file(hueLog.c_str());

        size_t errorsCnt = printData(file, hueFractIntMap, hueIntDataMap, hueBits, 3);
        if (errorsCnt > 0)
            fail = true;
        std::cout << "errors: " << errorsCnt << std::endl;

        file.close();
    }

    // unique HSV colors

    std::set<HSV> uniqueColors;

    for (uint32_t c = 0; c <= 0xFFFFFF; c++) {
        uint8_t redOrig = (c >> 16);
        uint8_t greenOrig = (c >> 8);
        uint8_t blueOrig = c;

        uint32_t hue;
        uint16_t saturation;
        uint8_t value;
        rgb2hsv(redOrig, greenOrig, blueOrig, hue, saturation, value);

        uniqueColors.insert(HSV(hue, saturation, value));
    }

    std::cout << "Unique HSV colors: " << uniqueColors.size();
    if (uniqueColors.size() != 256 * 256 * 256) {
        std::cout << " (not correct)";
        fail = true;
    }
    std::cout << std::endl;

    return fail ? 1 : 0;
}
