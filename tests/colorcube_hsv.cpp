/*
 * Performs forward and backward conversions between RGB and HSV spaces
 * for all possible colors from RGB colorcube using integer-based algorithm.
 */

#include <chrono>
#include <iostream>
#include <limits>
#include <stdint.h>
#include <stdio.h>

#include "../colormath/hsv.h"

int test() {
    using namespace colormath;

    for (uint32_t c = 0; c <= 0xFFFFFF; c++) {
        uint8_t redOrig = (c >> 16);
        uint8_t greenOrig = (c >> 8);
        uint8_t blueOrig = c;

        uint32_t hue;
        uint16_t saturation;
        uint8_t value;
        rgb2hsv(redOrig, greenOrig, blueOrig, hue, saturation, value);

        uint8_t redOut, greenOut, blueOut;
        hsv2rgb(hue, saturation, value, redOut, greenOut, blueOut);

        if (redOut != redOrig || greenOut != greenOrig || blueOut != blueOrig) {
            std::cerr << "original RGB ";
            std::cerr << "(" << (int)redOrig << ", " << (int)greenOrig << ", " << (int)blueOrig << ")";
            std::cerr << " != ";
            std::cerr << "(" << (int)redOut << ", " << (int)greenOut << ", " << (int)blueOut << ")";
            std::cerr << " for HSV ";
            std::cerr << "(" << (int)hue << ", " << (int)saturation << ", " << (int)value << ")";
            std::cerr << std::endl;
            return 1;
        }
    }

    return 0;
}

int main(int argc, char** argv) {
    int iterationsCount = 1;

    if (argc > 1)
    {
        char* iterationsCountVal = argv[1];
        sscanf(iterationsCountVal, "%d", &iterationsCount);
    }

    long long bestTime = std::numeric_limits<long long>::max();
    for (int i = 0; i < iterationsCount; i++) {
        auto start = std::chrono::high_resolution_clock::now();
        int rv = test();
        auto end = std::chrono::high_resolution_clock::now();
        if (rv != 0)
            return rv;

        auto dt = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
        if (dt < bestTime)
            bestTime = dt;

        std::cout << "Iteration " << (i + 1) << " time: " << dt << " ms" << std::endl;
    }

    std::cout << "Best time: " << bestTime << " ms" << std::endl;
    return 0;
}
