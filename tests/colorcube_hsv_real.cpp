/*
 * Performs forward and backward conversions between RGB and HSV spaces
 * for all possible colors from RGB colorcube using real-valued algorithm.
 */

#include <chrono>
#include <iostream>
#include <limits>
#include <stdint.h>
#include <stdio.h>

#include "../colormath/hsv_real.h"

int test() {
    using namespace colormath;

    for (uint32_t c = 0; c <= 0xFFFFFF; c++) {
        uint8_t redOrig = (c >> 16);
        uint8_t greenOrig = (c >> 8);
        uint8_t blueOrig = c;

        float redReal = (float)redOrig * (1.0f / 255.0f);
        float greenReal = (float)greenOrig * (1.0f / 255.0f);
        float blueReal = (float)blueOrig * (1.0f / 255.0f);

        float hueReal, saturationReal, valueReal;
        rgb2hsv_real(redReal, greenReal, blueReal, hueReal, saturationReal, valueReal);

        float redOutReal, greenOutReal, blueOutReal;
        hsv2rgb_real(hueReal, saturationReal, valueReal, redOutReal, greenOutReal, blueOutReal);

        uint8_t redOut = (uint8_t)(redOutReal * 255.0f + 0.5f);
        uint8_t greenOut = (uint8_t)(greenOutReal * 255.0f + 0.5f);
        uint8_t blueOut = (uint8_t)(blueOutReal * 255.0f + 0.5f);

        if (redOut != redOrig || greenOut != greenOrig || blueOut != blueOrig) {
            std::cerr << "original RGB ";
            std::cerr << "(" << (int)redOrig << ", " << (int)greenOrig << ", " << (int)blueOrig << ")";
            std::cerr << " != ";
            std::cerr << "(" << (int)redOut << ", " << (int)greenOut << ", " << (int)blueOut << ")";
            std::cerr << " for HSV ";
            std::cerr << "(" << hueReal << ", " << saturationReal << ", " << valueReal << ")";
            std::cerr << std::endl;
            return 1;
        }
    }

    return 0;
}

int main(int argc, char** argv) {
    int iterationsCount = 1;

    if (argc > 1)
    {
        char* iterationsCountVal = argv[1];
        sscanf(iterationsCountVal, "%d", &iterationsCount);
    }

    long long bestTime = std::numeric_limits<long long>::max();
    for (int i = 0; i < iterationsCount; i++) {
        auto start = std::chrono::high_resolution_clock::now();
        int rv = test();
        auto end = std::chrono::high_resolution_clock::now();
        if (rv != 0)
            return rv;

        auto dt = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
        if (dt < bestTime)
            bestTime = dt;

        std::cout << "Iteration " << (i + 1) << " time: " << dt << " ms" << std::endl;
    }

    std::cout << "Best time: " << bestTime << " ms" << std::endl;
    return 0;
}
