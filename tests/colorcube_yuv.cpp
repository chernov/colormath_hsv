/*
 * Performs forward and backward conversions between RGB and YUV spaces
 * for all possible colors from RGB colorcube.
 */

#include <iostream>
#include <stdint.h>
#include <stdlib.h>

#include "../colormath/yuv.h"

int main(int, char**) {
    using namespace colormath;

    int errorRed = 0;
    int errorGreen = 0;
    int errorBlue = 0;

    for (uint32_t c = 0; c <= 0xFFFFFF; c++) {
        uint8_t redOrig = (c >> 16);
        uint8_t greenOrig = (c >> 8);
        uint8_t blueOrig = c;

        uint8_t y, u, v;
        rgb2yuv(redOrig, greenOrig, blueOrig, y, u, v);

        uint8_t redOut, greenOut, blueOut;
        yuv2rgb(y, u, v, redOut, greenOut, blueOut);

        int diffRed = abs((int)redOut - (int)redOrig);
        if (diffRed > errorRed)
            errorRed = diffRed;

        int diffGreen = abs((int)greenOut - (int)greenOrig);
        if (diffGreen > errorGreen)
            errorGreen = diffGreen;

        int diffBlue = abs((int)blueOut - (int)blueOrig);
        if (diffBlue > errorBlue)
            errorBlue = diffBlue;
    }

    std::cout << "RGB to YUV conversion error: ";
    std::cout << "(" << errorRed << ", " << errorGreen << ", " << errorBlue << ")";
    std::cout << std::endl;

    return 0;
}
