/*
 * Tests equivalence of integer-based RGB to HSV conversion
 * with canonical real-valued conversion.
 */

#include <iostream>
#include <math.h>
#include <stdint.h>

#include "../colormath/hsv.h"
#include "../colormath/hsv_real.h"

int main(int, char**) {
    using namespace colormath;

    const float tolerance = 0.00002f;

    for (uint32_t c = 0; c <= 0xFFFFFF; c++) {
        uint8_t redOrig = (c >> 16);
        uint8_t greenOrig = (c >> 8);
        uint8_t blueOrig = c;

        uint32_t hue;
        uint16_t saturation;
        uint8_t value;
        rgb2hsv(redOrig, greenOrig, blueOrig, hue, saturation, value);

        float redReal = (float)redOrig * (1.0f / 255.0f);
        float greenReal = (float)greenOrig * (1.0f / 255.0f);
        float blueReal = (float)blueOrig * (1.0f / 255.0f);

        float hueReal, saturationReal, valueReal;
        rgb2hsv_real(redReal, greenReal, blueReal, hueReal, saturationReal, valueReal);

        float hueIntReal = (float)hue / maxHue;
        float saturationIntReal = (float)saturation / maxSaturation;
        float valueIntReal = (float)value / maxValue;

        float diffHue = fabsf(hueIntReal - hueReal);
        float diffSaturation = fabsf(saturationIntReal - saturationReal);
        float diffValue = fabsf(valueIntReal - valueReal);

        if (diffHue > tolerance || diffSaturation > tolerance || diffValue > tolerance) {
            std::cerr << "type casted ";
            std::cerr << "(" << hueIntReal << ", " << saturationIntReal << ", " << valueIntReal << ")";
            std::cerr << " != ";
            std::cerr << " real ";
            std::cerr << "(" << hueReal << ", " << saturationReal << ", " << valueReal << ")";
            std::cerr << " for HSV ";
            std::cerr << "(" << (int)hue << ", " << (int)saturation << ", " << (int)value << ")";
            std::cerr << std::endl;
            return 1;
        }
    }

    return 0;
}
